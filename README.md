# Deepso Client Side

Hi, this is one of my previous side project based on Vue.JS. It's called Deepso, which is a web application that allows users to add different websites or search engines and combine them into one search package for creating a customized search.

There are two main concepts for Deepso:

1. action: simply a website that people can search something
2. combo: bundle of different actions

e.g. A designer may want to search images from different websites, so he can create a combo which including dribble, unsplash and google images. Then he can search in these 3 platform at the same time.

e.g. If you want to buy a book, you may want to check the goodread, and then go to amazon.

Unfortunately, the project is not more available, and this repo is the front-end part of the project.

