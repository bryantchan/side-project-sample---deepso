import { buildUrl } from 'src/utils'

export default {
  beforeRouteEnter (to, from, next) {
    next((vm) => {
      vm.$trackr.pageView(vm.$route)
    })
  },
  methods: {
    toggleAuthPopup (status, from) {
      this.$store.dispatch('updateComponent', { name: 'authPopup', data: status, from })
      this.$trackr.track('authPopup', {
        from
      })
    },
    goSearch (query) {
      const url = `http://${window.location.host}/search`
      window.open(buildUrl(url, query))
    }
  }
}
