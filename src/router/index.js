import Vue from 'vue';
import Router from 'vue-router';
import routes from './routes'

Vue.use(Router);
const router = new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes
});

// router.afterEach(function (to, from) {
//   if (store.getters.isAuth && to.meta.guest) {
//     router.push('/')
//   }
//   if (to.meta.auth && !store.getters.isAuth) {
//     router.push('/auth/login')
//   }
// })
export default router
