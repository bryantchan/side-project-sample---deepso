/* eslint-disable global-require */
export default [
  {
    path: '/',
    name: 'home',
    meta: { track: true },
    component: require('views/home.vue')
  },
  {
    path: '/search',
    name: 'search',
    meta: { track: true },
    component: require('views/search/index.vue')
  },
  {
    path: '/auth',
    name: 'auth',
    component: require('views/auth/index.vue'),
    children: [
      {
        path: 'login',
        name: 'login',
        meta: { guest: true },
        component: require('views/auth/login.vue')
      },
      {
        path: 'logout',
        name: 'logout',
        meta: { auth: true },
        component: require('views/auth/logout.vue')
      },
      {
        path: 'register',
        name: 'register',
        meta: { guest: true },
        component: require('views/auth/register.vue')
      },
      {
        path: 'forget',
        name: 'forget',
        meta: { guest: true, track: true },
        component: require('views/auth/forget.vue')
      }
    ]
  },
  {
    path: '/gallery',
    name: 'gallery',
    meta: { track: true },
    component: require('views/gallery/index.vue'),
    children: [
      {
        path: 'list',
        name: 'gallery-list',
        component: require('views/gallery/list.vue')
      },
      {
        path: 'search',
        name: 'gallery-search',
        component: require('views/gallery/list.vue')
      },
      {
        path: 'tags/:tag_slug/:type',
        name: 'gallery-tags',
        component: require('views/gallery/tags.vue')
      },
      {
        path: 'catalogs/:cat_id/:type',
        name: 'gallery-catalogs',
        component: require('views/gallery/catalogs.vue')
      },
      {
        path: 'actions',
        component: require('views/gallery/actions/index.vue'),
        children: [
          {
            path: 'list',
            name: 'gallery-actions-list',
            component: require('views/gallery/actions/list.vue')
          }
        ]
      },
      {
        path: 'combos',
        component: require('views/gallery/combos/index.vue'),
        children: [
          {
            path: 'list',
            name: 'gallery-combos-list',
            component: require('views/gallery/combos/list.vue')
          }
        ]
      }
    ]
  },
  {
    path: '/gallery/actions/:action_id',
    name: 'gallery-action',
    meta: { track: true },
    component: require('views/gallery/actions/item.vue')
  },
  {
    path: '/gallery/combos/:combo_id',
    name: 'gallery-combo',
    meta: { track: true },
    component: require('views/gallery/combos/item.vue')
  },
  {
    path: '/users',
    component: require('views/users/index.vue'),
    children: [
      {
        path: 'account',
        name: 'users-account',
        meta: { auth: true, track: true },
        component: require('views/users/account.vue')
      },
      {
        path: 'gallery',
        component: require('views/users/gallery/index.vue'),
        redirect: { path: '/users/gallery/follow' },
        children: [
          {
            path: 'follow',
            meta: { auth: true },
            component: require('views/users/gallery/follow/index.vue'),
            redirect: { path: '/users/gallery/follow/actions' },
            children: [
              {
                path: 'actions',
                name: 'users-follow-actions',
                meta: { auth: true, track: true },
                component: require('views/users/gallery/follow/actions.vue')
              },
              {
                path: 'combos',
                name: 'users-follow-combos',
                meta: { auth: true, track: true },
                component: require('views/users/gallery/follow/combos.vue')
              }
            ]
          },
          {
            path: 'follow/actions/:action_id/edit',
            name: 'users-follow-actions-edit',
            meta: { auth: true, track: true },
            component: require('views/users/gallery/follow/actions-edit.vue')
          },
          {
            path: 'follow/combos/:combo_id/edit',
            name: 'users-follow-combos-edit',
            meta: { auth: true, track: true },
            component: require('views/users/gallery/follow/combos-edit.vue')
          },
          {
            path: 'own',
            meta: { auth: true },
            component: require('views/users/gallery/own/index.vue'),
            redirect: { path: '/users/gallery/own/actions' },
            children: [
              {
                path: 'actions',
                name: 'users-own-actions',
                meta: { auth: true, track: true },
                component: require('views/users/gallery/own/actions.vue')
              },
              {
                path: 'combos',
                name: 'users-own-combos',
                meta: { auth: true, track: true },
                component: require('views/users/gallery/own/combos.vue')
              }
            ]
          },
          {
            path: 'own/actions/create',
            name: 'users-create-action',
            meta: { auth: true, track: true },
            component: require('views/users/gallery/actions/create.vue')
          },
          {
            path: 'own/actions/popup/:url?',
            name: 'users-create-action-popup',
            meta: { auth: true, track: true },
            component: require('views/users/gallery/actions/popup.vue')
          },
          {
            path: 'own/combos/create/:ids?',
            name: 'users-create-combo',
            meta: { auth: true, track: true },
            component: require('views/users/gallery/combos/create.vue')
          },
          {
            path: 'own/actions/:action_id/edit',
            name: 'users-edit-action',
            meta: { auth: true, track: true },
            component: require('views/users/gallery/actions/edit.vue')
          },
          {
            path: 'own/combos/:combo_id/edit',
            name: 'users-edit-combo',
            meta: { auth: true, track: true },
            component: require('views/users/gallery/combos/edit.vue')
          }
        ]
      }
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
]
