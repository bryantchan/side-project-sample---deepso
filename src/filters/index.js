const host = (url) => {
  const hostname = url.replace(/^https?:\/\//, '').replace(/\/.*$/, '');
  const parts = hostname.split('.').slice(-3);
  if (parts[0] === 'www') parts.shift();
  return parts.join('.');
}

export default { host }
