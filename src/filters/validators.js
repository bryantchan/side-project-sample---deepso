/* eslint-disable import/prefer-default-export */
const validUrl = str => /^(ftp|http|https):\/\/[^ "]+$/.test(str) && str.indexOf('[keyword]') > -1

export { validUrl }
