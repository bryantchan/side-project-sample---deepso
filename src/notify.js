/* eslint-disable max-len */
import { ONESIGNAL_APPID, ONESIGNAL_SAFARI_ID } from 'src/config'

const textConf = {
  'tip.state.unsubscribed': '想获取最新的资源和搜索技巧？',
  'tip.state.subscribed': '你已订阅推送',
  'tip.state.blocked': '你已屏蔽订阅推送',
  'message.prenotify': '想获取最新的资源和搜索技巧？',
  'message.action.subscribed': '感谢订阅！',
  'message.action.resubscribed': '你已订阅推送',
  'message.action.unsubscribed': '好遗憾，你离信息自由又远了一步！',
  'dialog.main.title': '管理 Deepso 推送',
  'dialog.main.button.subscribe': '试一下',
  'dialog.main.button.unsubscribe': '取消推送',
  'dialog.blocked.title': '取消屏蔽',
  'dialog.blocked.message': '请按照提示完成操作:'
}

const btnColor = { // Customize the colors of the main button and dialog popup button
  'circle.background': '#19A6FF',
  'circle.foreground': 'white',
  'badge.background': '#19A6FF',
  'badge.foreground': 'white',
  'badge.bordercolor': 'white',
  'pulse.color': 'white',
  'dialog.button.background.hovering': '#2F4460',
  'dialog.button.background.active': '#2F4460',
  'dialog.button.background': '#19A6FF',
  'dialog.button.foreground': 'white'
}

const welcomeNotify = {
  title: '感谢订阅 Deepso',
  message: '不打扰，只有优质的搜索资源和技巧！'
  // "url": "" /* Leave commented for the notification to not open a window on Chrome and Firefox (on Safari, it opens to your webpage) */
}

const options = {
  appId: ONESIGNAL_APPID,
  safari_web_id: ONESIGNAL_SAFARI_ID,
  autoRegister: false, /* Set to true to automatically prompt visitors */
  /*
   subdomainName: Use the value you entered in step 1.4: http://imgur.com/a/f6hqN
   */
  httpPermissionRequest: {
    enable: true
  },
  notifyButton: {
    enable: true, /* Set to false to hide */
    size: 'small', /* One of 'small', 'medium', or 'large' */
    theme: 'default', /* One of 'default' (red-white) or 'inverse" (white-red) */
    position: 'bottom-right', /* Either 'bottom-left' or 'bottom-right' */
    offset: {
      bottom: '20px',
      right: '20px' /* Only applied if bottom-right */
    },
    prenotify: true, /* Show an icon with 1 unread message for first-time site visitors */
    showCredit: false, /* Hide the OneSignal logo */
    text: textConf,
    colors: btnColor
  },
  welcomeNotification: welcomeNotify
}

if (process.env.NODE_ENV !== 'production') {
  options.subdomainName = 'deepsodev'
}

/* eslint-disable max-len */
export default {
  init () {
    const OneSignal = window.OneSignal || []
    OneSignal.push(['init', options])
  }
}
