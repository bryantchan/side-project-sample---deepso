/* eslint-disable import/prefer-default-export */
const buildUrl = (url, parameters) => {
  let qs = ''
  let str = url
  Object.keys(parameters).forEach((key) => {
    if (Object.prototype.hasOwnProperty.call(parameters, key)) {
      const value = parameters[key]
      qs += `${encodeURIComponent(key)}=${encodeURIComponent(value)}&`
    }
  })
  if (qs.length > 0) {
    qs = qs.substring(0, qs.length - 1) // chop off last "&"
    str = `${str}?${qs}`
  }
  return str
}
export { buildUrl }
