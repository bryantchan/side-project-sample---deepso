import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueI18n from 'vue-i18n'
import { sync } from 'vuex-router-sync'
import Vuelidate from 'vuelidate'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'

import App from './App'
import router from './router'
import store from './store'
import mixin from './mixin'
import locales from './langs/index'
import * as filters from './filters'
import httpConf from './http'
import Trackr from './trackr'


require('../static/css/reset.css')
require('../static/css/grid.css')
require('../static/css/font.css')

// sync the router with the vuex store.
// this registers `store.state.route`
sync(store, router)

// install plugins
Vue.use(Vuelidate)
Vue.use(VueAxios, httpConf)
Vue.use(VueI18n)
Vue.use(Trackr)
Vue.config.lang = 'zh'

Vue.mixin(mixin)

// set locales
Object.keys(locales).forEach((lang) => {
  Vue.locale(lang, locales[lang])
})

// register global utility filters.
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})

Raven
  .config('https://20579a7e983945a5a97b861630df4077@sentry.io/154351')
  .addPlugin(RavenVue, Vue)
  .install()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
