import { STORAGE_PREFIX } from '@/config'
import Cookie from 'js-cookie'

export default {
  get (key) {
    const value = Cookie.get(STORAGE_PREFIX + key)
    return value ? JSON.parse(value) : false
  },
  set (key, val) {
    const value = JSON.stringify(val)
    Cookie.set(STORAGE_PREFIX + key, value, { expires: 7, secure: true }) // https
    Cookie.set(STORAGE_PREFIX + key, value, { expires: 7 }) // http
  },
  remove (key) {
    Cookie.remove(STORAGE_PREFIX + key)
    Cookie.remove(STORAGE_PREFIX + key, { secure: true })
  }
}
