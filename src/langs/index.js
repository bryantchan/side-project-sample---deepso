import zh from './zh/index'
import en from './en/index'

export default {
  zh,
  en
}
