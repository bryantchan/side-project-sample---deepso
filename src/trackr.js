/* eslint-disable no-param-reassign */
import { MIXPANEL_KEY, GA_KEY } from 'src/config'

export default {
  install (Vue) {
    Vue.prototype.$trackr = {
      init (callback) {
        if (GA_KEY) {
          window.ga('create', GA_KEY, 'auto')
          window.ga('send', 'pageview')
        }
        window.mixpanel.init(MIXPANEL_KEY, {
          loaded: callback
        })
      },
      pageView (route) {
        if (route.meta && route.meta.track) {
          window.mixpanel.track('page-view', {
            name: route.name
          })
        }
      },
      track (event, option, cb) {
        return window.mixpanel.track(event, option, cb)
      },
      identify (user) {
        const userId = user.id ? user.id : window.mixpanel.get_distinct_id()
        window.mixpanel.identify(userId)
        window.mixpanel.register({
          auth: user.id ? 1 : 0
        })
      },
      alias (user) {
        return window.mixpanel.alias(user.id, window.mixpanel.get_distinct_id())
      },
      profile (option) {
        window.mixpanel.people.set({
          $name: option.name,
          $email: option.email
        })
      }
    }
  }
}
