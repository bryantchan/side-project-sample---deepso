export const logos = [
  {
    slug: 'wechat weixin 微信',
    class: 'ds_logo logo-wechat'
  },
  {
    slug: 'imdb',
    class: 'ds_logo logo-imdb'
  },
  {
    slug: 'quora',
    class: 'ds_logo logo-quora'
  },
  {
    slug: 'ebay',
    class: 'ds_logo logo-ebay'
  },
  {
    slug: 'dribbble',
    class: 'ds_logo logo-dribbble'
  },
  {
    slug: 'behance',
    class: 'ds_logo logo-behance'
  },
  {
    slug: '36kr 36氪',
    class: 'ds_logo logo-36kr'
  },
  {
    slug: 'buzzfeed',
    class: 'ds_logo logo-buzzfeed'
  },
  {
    slug: '苹果 apple',
    class: 'ds_logo logo-apple'
  },
  {
    slug: 'youtube',
    class: 'ds_logo logo-youtube'
  },
  {
    slug: 'twitch',
    class: 'ds_logo logo-twitch'
  },
  {
    slug: 'vimeo',
    class: 'ds_logo logo-vimeo'
  },
  {
    slug: 'qq 腾讯 企鹅',
    class: 'ds_logo logo-qq'
  },
  {
    slug: 'A站 acfun',
    class: 'ds_logo logo-acfun'
  },
  {
    slug: 'B站 bilibili',
    class: 'ds_logo logo-bilibili'
  },
  {
    slug: 'mtime 时光 时光网',
    class: 'ds_logo logo-mtime'
  },
  {
    slug: 'reddit',
    class: 'ds_logo logo-reddit'
  },
  {
    slug: 'facebook',
    class: 'ds_logo logo-facebook'
  },
  {
    slug: 'twitter 推特',
    class: 'ds_logo logo-twitter'
  },
  {
    slug: 'tumblr',
    class: 'ds_logo logo-tumblr'
  },
  {
    slug: 'medium',
    class: 'ds_logo logo-medium'
  },
  {
    slug: 'weibo 微博',
    class: 'ds_logo logo-weibo'
  },
  {
    slug: 'douban 豆瓣',
    class: 'ds_logo logo-douban'
  },
  {
    slug: 'tmall 天猫 tianmao',
    class: 'ds_logo logo-tianmao'
  },
  {
    slug: 'dangdang 当当',
    class: 'ds_logo logo-dangdang'
  },
  {
    slug: 'amazon 亚马逊',
    class: 'ds_logo logo-amazon'
  },
  {
    slug: '1号店 1haodian',
    class: 'ds_logo logo-yihaodian'
  }, {
    slug: 'jd 京东',
    class: 'ds_logo logo-jingdong'
  },
  {
    slug: '淘宝 taobao 阿里巴巴',
    class: 'ds_logo logo-taobao'
  },
  {
    slug: '知乎 zhihu',
    class: 'ds_logo logo-zhihu'
  },
  {
    slug: 'google fanyi 翻译 translate',
    class: 'ds_logo logo-translate'
  },
  {
    slug: '有道 youdao',
    class: 'ds_logo logo-youdao'
  },
  {
    slug: '丁香园 丁香 dingxiang',
    class: 'ds_logo logo-dingxiangke'
  },
  {
    slug: '360',
    class: 'ds_logo logo-360'
  },
  {
    slug: 'baidu 百度',
    class: 'ds_logo logo-baidu'
  },
  {
    slug: '搜狗 sougou sogou',
    class: 'ds_logo logo-sougou'
  },
  {
    slug: '雅虎 yahoo',
    class: 'ds_logo logo-yahoo'
  },
  {
    slug: 'bing 必应',
    class: 'ds_logo logo-bing'
  },
  {
    slug: '谷歌 google',
    class: 'ds_logo logo-google'
  },
  {
    slug: '搜狐 sohu',
    class: 'ds_logo logo-souhushipin'
  },
  {
    slug: '网易 网易音乐 音乐 网易云音乐 music',
    class: 'ds_logo logo-wangyimusic'
  },
  {
    slug: '虾米 虾米音乐 音乐 music',
    class: 'ds_logo logo-xiami'
  },
  {
    slug: 'spotify spotify音乐 音乐 music',
    class: 'ds_logo logo-spotify'
  },
  {
    slug: 'qq qq音乐 音乐 music',
    class: 'ds_logo logo-qqyinyue'
  }
]
export const daily = [
  {
    slug: 'bt xiazai download 下载 种子',
    class: 'ds_icon icon-xiazai'
  },
  {
    slug: '二次元 erciyuan dongman 动漫',
    class: 'ds_icon icon-erciyuan'
  },
  {
    slug: 'xiazai 下载 磁链 cilian 种子 zhongzi torrent',
    class: 'ds_icon icon-cilian'
  },
  {
    slug: '弹幕 danmu 字幕 zimu 评论 pinglun',
    class: 'ds_icon icon-danmu'
  },
  {
    slug: '电视 dianshi tv',
    class: 'ds_icon icon-dianshi'
  },
  {
    slug: 'xiazai 下载 bt 种子 zhongzi torrent',
    class: 'ds_icon icon-torrent'
  },
  {
    slug: '电视 dianshi tv',
    class: 'ds_icon icon-dianshiju'
  },
  {
    slug: 'gouwu 购物 shopping',
    class: 'ds_icon icon-shopping'
  },
  {
    slug: 'image picture 图片 照片',
    class: 'ds_icon icon-tupian'
  },
  {
    slug: '摄影 拍照 相机 photo camera',
    class: 'ds_icon icon-sheying'
  },
  {
    slug: '设计 design',
    class: 'ds_icon icon-weibiaoti2'
  },
  {
    slug: '设计 design',
    class: 'ds_icon icon-sheji1'
  },
  {
    slug: 'gongsi 房子 楼 house building',
    class: 'ds_icon icon-gongsi'
  },
  {
    slug: '设计 design',
    class: 'ds_icon icon-sheji'
  },
  {
    slug: '钱 纸币 币 money 经济 商业',
    class: 'ds_icon icon-jingji'
  },
  {
    slug: '钱 纸币 币 money 经济 商业',
    class: 'ds_icon icon-dollar'
  },
  {
    slug: '学术 毕业 学士 帽 论文',
    class: 'ds_icon icon-xueshimao'
  },
  {
    slug: '车 car 驾驶 drive',
    class: 'ds_icon icon-car'
  },
  {
    slug: '旅行 travel 行李 行李箱',
    class: 'ds_icon icon-lvxing'
  },
  {
    slug: '航班 飞机 旅行 plane travel 旅途',
    class: 'ds_icon icon-1'
  },
  {
    slug: 'app 应用',
    class: 'ds_icon icon-yingyong'
  },
  {
    slug: 'app 应用',
    class: 'ds_icon icon-appstore'
  },
  {
    slug: 'app 应用 手机 mobile',
    class: 'ds_icon icon-app'
  },
  {
    slug: 'data 图表 数据',
    class: 'ds_icon icon-tubiao'
  },
  {
    slug: '新闻 news 报纸',
    class: 'ds_icon icon-xinwen1'
  },
  {
    slug: '新闻 news 报纸 博客',
    class: 'ds_icon icon-boke'
  },
  {
    slug: '标签 tag',
    class: 'ds_icon icon-tag'
  },
  {
    slug: '新闻 news 报纸 博客',
    class: 'ds_icon icon-xinwen2'
  },
  {
    slug: '硬件 电脑 hardware 台式',
    class: 'ds_icon icon-yingjian'
  },
  {
    slug: '硬件 电脑 hardware 台式 mac 显示器 displayer',
    class: 'ds_icon icon-display'
  },
  {
    slug: '视频 video shipin 电影 点播 live 直播 movie film',
    class: 'ds_icon icon-shipin'
  },
  {
    slug: '视频 video shipin 电影 点播 live 直播 movie film',
    class: 'ds_icon icon-dianying2-12'
  },
  {
    slug: '视频 video shipin 电影 点播 live 直播 movie film',
    class: 'ds_icon icon-dianying1'
  },
  {
    slug: '视频 video shipin 电影 点播 live 直播 movie film',
    class: 'ds_icon icon-dianying'
  },
  {
    slug: '视频 video shipin 电影 点播 live 直播 movie film',
    class: 'ds_icon icon-dianying3'
  },
  {
    slug: '新闻 news 报纸 博客',
    class: 'ds_icon icon-xinwen'
  },
  {
    slug: '鞋子 潮流 shoes 包包',
    class: 'ds_icon icon-xiezi'
  },
  {
    slug: '手机 mobile iphone andriod',
    class: 'ds_icon icon-shouji'
  },
  {
    slug: '邮件 邮编 mail 信封',
    class: 'ds_icon icon-youjian'
  },
  {
    slug: '用户 user',
    class: 'ds_icon icon-yonghu'
  },
  {
    slug: '问答 百科 知识 知乎 评论',
    class: 'ds_icon icon-baike'
  },
  {
    slug: 'dict 字典 翻译 translate',
    class: 'ds_icon icon-dict'
  },
  {
    slug: 'dict 字典 翻译 translate',
    class: 'ds_icon icon-fanyi'
  },
  {
    slug: '医疗 医生 医学 用药 药',
    class: 'ds_icon icon-yisheng'
  },
  {
    slug: '医疗 医生 医学 用药 药',
    class: 'ds_icon icon-yaowan'
  },
  {
    slug: '搜索 search',
    class: 'ds_icon icon-search1'
  },
  {
    slug: '日本 Japen',
    class: 'ds_icon icon-ribenguan'
  },
  {
    slug: '美国 America US',
    class: 'ds_icon icon-meiguoguan'
  },
  {
    slug: '韩国 Korea',
    class: 'ds_icon icon-hanguoguan'
  },
  {
    slug: '音乐 music 音符',
    class: 'ds_icon icon-yinyue'
  },
  {
    slug: '音乐 music 音符',
    class: 'ds_icon icon-yinyue2'
  },
  {
    slug: '音乐 music 音符',
    class: 'ds_icon icon-music'
  }
]
export const alphabet = [
  {
    slug: 'a',
    class: 'ds_alphabet icon-zimua'
  },
  {
    slug: 'b',
    class: 'ds_alphabet icon-zimub'
  },
  {
    slug: 'c',
    class: 'ds_alphabet icon-zimuc'
  },
  {
    slug: 'd',
    class: 'ds_alphabet icon-zimud'
  },
  {
    slug: 'e',
    class: 'ds_alphabet icon-zimue'
  },
  {
    slug: 'f',
    class: 'ds_alphabet icon-zimuf'
  },
  {
    slug: 'g',
    class: 'ds_alphabet icon-zimug'
  },
  {
    slug: 'h',
    class: 'ds_alphabet icon-zimuh'
  },
  {
    slug: 'i',
    class: 'ds_alphabet icon-zimui'
  },
  {
    slug: 'j',
    class: 'ds_alphabet icon-zimuj'
  },
  {
    slug: 'k',
    class: 'ds_alphabet icon-zimuk'
  },
  {
    slug: 'l',
    class: 'ds_alphabet icon-zimul'
  },
  {
    slug: 'm',
    class: 'ds_alphabet icon-zimum'
  },
  {
    slug: 'n',
    class: 'ds_alphabet icon-zimun'
  },
  {
    slug: 'o',
    class: 'ds_alphabet icon-zimuo'
  },
  {
    slug: 'p',
    class: 'ds_alphabet icon-zimup'
  },
  {
    slug: 'q',
    class: 'ds_alphabet icon-zimuq'
  },
  {
    slug: 'r',
    class: 'ds_alphabet icon-zimur'
  },
  {
    slug: 's',
    class: 'ds_alphabet icon-zimus'
  },
  {
    slug: 't',
    class: 'ds_alphabet icon-zimut'
  },
  {
    slug: 'u',
    class: 'ds_alphabet icon-zimuu'
  },
  {
    slug: 'v',
    class: 'ds_alphabet icon-zimuv'
  },
  {
    slug: 'w',
    class: 'ds_alphabet icon-zimuw'
  },
  {
    slug: 'x',
    class: 'ds_alphabet icon-zimux'
  },
  {
    slug: 'y',
    class: 'ds_alphabet icon-zimuy'
  },
  {
    slug: 'z',
    class: 'ds_alphabet icon-zimuz'
  }
]
