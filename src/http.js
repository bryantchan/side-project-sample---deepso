/* eslint-disable no-param-reassign,no-else-return,consistent-return,prefer-arrow-callback */
import axios from 'axios'
import { API_HOST } from './config'
import db from './db'

axios.defaults.baseURL = API_HOST

// Add a request interceptor
axios.interceptors.request.use((config) => {
  const token = db.get('token')
  if (token) {
    config.url = `${config.url}?token=${token}`
  }
  // Do something before request is sent
  config.validateStatus = () => true
  return config
}, (error) => {
  // Do something with request error
  Promise.reject(error)
})
// Add a response interceptor
axios.interceptors.response.use((response) => {
  if (response.status === 500 && response.data.message.indexOf('Unauthenticated') > -1) {
    window.location.href = '/auth/login'
  } else if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    return Promise.reject(response)
  }
}, (error) => {
  // Do something with request error
  Promise.reject(error)
})

export default axios
