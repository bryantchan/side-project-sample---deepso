/* eslint-disable no-shadow,no-param-reassign */
import * as types from '../mutation-types'
// initial state
const state = {
  settings: {
    locale: 'zh'
  }
}
// getters
const getters = {
  activeLocale (state) {
    return state.settings.locale;
  }
}
// actions
const actions = {
  SET_LOCALE ({ commit }, locale) {
    commit(types.SET_LOCALE, locale);
  }
}
// mutations
const mutations = {
  [types.SET_LOCALE] (state, locale) {
    state.settings.locale = locale;
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
