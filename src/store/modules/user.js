/* eslint-disable no-shadow,no-param-reassign */
import Vue from 'vue'
import db from 'src/db'
import * as types from '../mutation-types'

// initial state
const state = {
  auth: {
    user: null,
    token: null
  },
  components: {
    authPopup: false,
    extension: false,
    homePicker: false
  }
}
// getters
const getters = {
  components (state) {
    return state.components
  },
  userinfo (state) {
    return state.auth.user;
  },
  isAuth () {
    return !!db.get('token')
  },
  token (state) {
    return state.auth.token
  }
}

// actions
const actions = {
  init ({ commit }) {
    return new Promise((resolve) => {
      const user = db.get('user')
      const token = db.get('token')
      const authData = { user, token }
      if (user && token) {
        commit(types.SET_AUTH_DATA, authData)
        Vue.axios.defaults.headers.common.Authorization = `Bearer ${token}`
      }
      resolve()
    })
  },
  login ({ commit }, data) {
    const user = data.data
    const token = data.meta.jwt_token
    const authData = { user, token }
    return new Promise((resolve) => {
      db.set('user', user)
      db.set('token', token)
      commit(types.SET_AUTH_DATA, authData)
      resolve()
    })
  },
  logout ({ commit }) {
    return new Promise((resolve) => {
      db.remove('user')
      db.remove('token')
      commit(types.UNSET_AUTH_DATA);
      resolve()
    })
  },
  updateComponent ({ commit, getters, dispatch }, component) {
    commit(types.UPDATE_COMPONENT, component)
  }
}
// mutations
const mutations = {
  [types.UPDATE_COMPONENT] (state, component) {
    state.components[component.name] = component.data
  },
  [types.SET_AUTH_DATA] (state, authData) {
    return new Promise((resolve) => {
      state.auth.user = authData.user
      state.auth.token = authData.token
      resolve()
    })
  },
  [types.UNSET_AUTH_DATA] (state) {
    state.auth.user = null
    state.auth.token = null
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
