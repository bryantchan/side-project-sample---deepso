/* eslint-disable no-shadow,no-param-reassign */
import * as types from '../mutation-types'
// initial state
const state = {
  sources: [],
  keyword: ''
}
// getters
const getters = {
// eslint-disable-next-line no-shadow
  sources (state) {
    return state.sources
  },
  keyword (state) {
    return state.keyword
  }
}
// actions
const actions = {
  setSources ({ commit }, sources) {
    commit(types.SET_SOURCES, sources)
  },
  removeSources ({ commit }) {
    commit(types.SET_SOURCES)
  },
  setKeyword ({ commit }, keyword) {
    commit(types.SET_KEYWORD, keyword)
  }
}
// mutations
const mutations = {
  [types.SET_SOURCES] (state, sources) {
    state.sources = sources
  },
  [types.REMOVE_SOURCES] (state) {
    state.sources = []
  },
  [types.SET_KEYWORD] (state, keyword) {
    state.keyword = keyword
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
