/* eslint-disable no-shadow,no-param-reassign */
import Users from 'api/users'
import Pack from 'api/pack'
import * as types from '../mutation-types'

// initial state
const state = {
  pack: {
    combos: [],
    actions: []
  }
}
// getters
const getters = {
  pack (state) {
    return state.pack
  }
}
// actions
const actions = {
  fetchPackage ({ commit }) {
    return new Promise((resolve, reject) => {
      Users.fetchPack().then((pack) => {
        commit(types.SET_PACKAGE, pack)
        resolve()
      }).catch((error) => {
        reject(error)
      })
    })
  },
  updateSource ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const type = payload.type
      const data = payload.data
      const id = payload.source.id
      Pack[type].follow.update(id, data).then(({ data: { data: newSource } }) => {
        commit(types.UPDATE_SOURCE, {
          type: payload.type,
          source: newSource
        })
        resolve(newSource)
      }).catch((error) => {
        reject(error)
      })
    })
  }
}
// mutations
const mutations = {
  [types.SET_PACKAGE] (state, pack) {
    state.pack = pack
  },
  [types.UPDATE_SOURCE] (state, payload) {
    state.pack[payload.type].forEach((source, index) => {
      if (source.id === payload.source.id) {
        // state.pack[payload.type][index] = payload.source
        state.pack[payload.type].splice(index, 1)
        state.pack[payload.type].push(payload.source)
      }
    })
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
