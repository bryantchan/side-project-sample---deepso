import Vue from 'vue'
import Vuex from 'vuex'
import locales from './modules/locales'
import user from './modules/user'
import search from './modules/search'
import packs from './modules/packs'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
  modules: {
    locales,
    search,
    packs,
    user
  },
  strict: debug
})
