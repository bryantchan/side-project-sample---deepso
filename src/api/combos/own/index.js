import Vue from 'vue'

const END_POINT = '/combos/own'

export default {
  /**
   * create a new combo
   * @param data
   * data
   - title | string
   - desc | string
   - shortcut | string
   - icon | string
   - color | string
   - shared | integer
   - pinned | integer
   - sort | integer
   - actions | array[integer]
   * @returns {AxiosPromise}
   */
  create (data) {
    return Vue.axios.post(END_POINT, data)
  },
  /**
   * update a combo
   * @param comboId
   * @param data
   * data
   - title | string
   - desc | string
   - shortcut | string
   - icon | string
   - color | string
   - shared | integer
   - pinned | integer
   - sort | integer
   - owner_id | integer 创建者 id
   - actions | array[integer]
   * @returns {AxiosPromise}
   * @returns {AxiosPromise}
   */
  update (comboId, data) {
    return Vue.axios.put(`${END_POINT}/${comboId}`, data)
  },
  getAll (params = {}) {
    return Vue.axios.get(END_POINT, {
      params
    })
  },
  get (comboId) {
    return Vue.axios.get(`${END_POINT}/${comboId}`)
  },
  remove (comboId) {
    return Vue.axios.delete(`${END_POINT}/${comboId}`)
  }
}
