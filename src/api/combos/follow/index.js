import Vue from 'vue'

const END_POINT = '/combos/follow'

export default {
  create (comboId, alias, shortcut) {
    return Vue.axios.post(END_POINT, {
      combo_id: comboId,
      alias,
      shortcut
    })
  },
  update (comboId, data) {
    return Vue.axios.put(`${END_POINT}/${comboId}`, data)
  },
  get (comboId) {
    return Vue.axios.get(`${END_POINT}/${comboId}`)
  },
  remove (comboId) {
    return Vue.axios.delete(`${END_POINT}/${comboId}`)
  },
  getAll () {
    return Vue.axios.get(END_POINT)
  },
  upgrade (actionIds) {
    return Vue.axios.put(`${END_POINT}/upgrade`, {
      action_ids: actionIds
    })
  }
}
