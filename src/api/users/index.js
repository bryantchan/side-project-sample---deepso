import Vue from 'vue'

export default {
  fetchPack () {
    return new Promise((resolve, reject) => {
      Promise.all([Vue.axios.get('/home/actions'), Vue.axios.get('/home/combos')]).then(([{ data: { data: actions } }, { data: { data: combos } }]) => {
        resolve({
          actions,
          combos
        })
      }).catch((error) => {
        reject(error)
      })
    })
  },
  update (user) {
    return Vue.axios.put('/user/profile', user)
  }
}
