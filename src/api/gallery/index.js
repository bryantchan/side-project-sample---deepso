import Vue from 'vue'
import Catalogs from './catalogs'

const Gallery = {
  catalogs: Catalogs,
  getByKeyword (type, keyword) {
    return Vue.axios.get(`/${type}`, {
      params: {
        keyword,
        order_by: 'sort',
        sort: 'asc',
        per_page: 20
      }
    })
  },
  getByCatId (type, catId) {
    return Vue.axios.get(`/${type}`, {
      params: {
        catalog_ids: catId,
        order_by: 'sort',
        sort: 'asc',
        per_page: 'all'
      }
    })
  },
  getByTagSlug (type, slug) {
    return Vue.axios.get(`/${type}`, {
      params: {
        tag: slug,
        order_by: 'sort',
        sort: 'asc',
        per_page: 'all'
      }
    })
  },
  getAll (type, params = {}) {
    return Vue.axios.get(`/${type}`, {
      params
    })
  },
  search (type, keyword) {
    return Vue.axios.get(`/${type}`, {
      params: {
        keyword
      }
    })
  },
  getById (type, id) {
    return Vue.axios.get(`/${type}/${id}`)
  },
  fetchPack () {
    return new Promise((resolve, reject) => Promise.all([Gallery.getByTagSlug('actions', 'default'), Gallery.getByTagSlug('combos', 'default')]).then(([{ data: { data: actions } }, { data: { data: combos } }]) => {
      resolve({
        actions,
        combos
      })
    }).catch((error) => {
      reject(error)
    }))
  }
}

export default Gallery
