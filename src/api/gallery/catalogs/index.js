import Vue from 'vue'

const END_POINT = '/catalogs'

export default {
  /**
   * 获取源的类别
   * @param params
   * @param type main|roles
   */
  getType (params, type = 'main') {
    return Vue.axios.get(`${END_POINT}/${type}`, {
      params
    })
  },
  getAll (params = {}) {
    const vm = this
    return new Promise((resolve, reject) => {
      Promise.all([vm.getType(params), vm.getType(params, 'roles')]).then(([{ data: { data: catalogs } }, { data: { data: roles } }]) => {
        resolve({
          catalogs,
          roles
        })
      }).catch((error) => {
        reject(error)
      })
    })
  }
}
