import actions from 'api/actions'
import combos from 'api/combos'

export default {
  actions,
  combos
}
