import Vue from 'vue'

const END_POINT = '/actions/own'
export default {
  /**
   * create a new action
   * @param data
   * data
   - title | string
   - desc | string
   - url | string
   - type | string url/api
   - target | string bla
   - shortcut | string
   - icon | string
   - color | string
   - shared | integer
   - pinned | integer
   - sort | integer
   - params | object
   * @returns {AxiosPromise}
   */
  create (data) {
    return Vue.axios.post(END_POINT, data)
  },
  /**
   * update a action
   * @param data
   * data
   - title | string
   - desc | string
   - shortcut | string
   - icon | string
   - color | string
   - shared | integer
   - pinned | integer
   - sort | integer
   - params | object
   * @returns {AxiosPromise}
   * @returns {AxiosPromise}
   */
  update (actionId, data) {
    return Vue.axios.put(`${END_POINT}/${actionId}`, data)
  },
  getAll (params = {}) {
    return Vue.axios.get(END_POINT, {
      params
    })
  },
  get (actionId) {
    return Vue.axios.get(`${END_POINT}/${actionId}`)
  },
  remove (actionId) {
    return Vue.axios.delete(`${END_POINT}/${actionId}`)
  }
}
