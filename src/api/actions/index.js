import Follow from './follow'
import Own from './own'

export default {
  own: Own,
  follow: Follow
}
