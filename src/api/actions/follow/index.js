import Vue from 'vue'

const END_POINT = '/actions/follow'
export default {
  create (actionId) {
    return Vue.axios.post(END_POINT, {
      action_id: actionId
    })
  },
  /**
   * 更新自己关注源的一些信息
   * @param actionId
   * @param data
   * @returns {AxiosPromise}
   */
  update (actionId, data) {
    return Vue.axios.put(`${END_POINT}/${actionId}`, data)
  },
  getAll (params = null) {
    return Vue.axios.get(END_POINT, { params })
  },
  get (actionId) {
    return Vue.axios.get(`${END_POINT}/${actionId}`)
  },
  remove (actionId) {
    return Vue.axios.delete(`${END_POINT}/${actionId}`)
  },
  /**
   * 更新关注的源
   * @param actionIds
   * @returns {AxiosPromise}
   */
  upgrade (actionIds) {
    return Vue.axios.put(`${END_POINT}/upgrade`, {
      action_ids: actionIds
    })
  }
}
