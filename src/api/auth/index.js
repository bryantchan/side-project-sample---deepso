import Vue from 'vue'

export default {
  register (auth) {
    return Vue.axios.post('/register', auth)
  },
  login (email, password) {
    return Vue.axios.post('/login', {
      email,
      password
    })
  },
  logout () {
    return Vue.axios.get('/logout')
  },
  reset (email) {
    return Vue.axios.post('/password/email', {
      email
    })
  }
}
