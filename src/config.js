// current host
export const HOST = process.env.NODE_ENV === 'production' ? 'https://deepso.io' : 'http://deepso.dev'

// for localstorage
export const STORAGE_PREFIX = process.env.NODE_ENV === 'production' ? 'deeps-prod-' : 'deepso-dev-env-'

// api endpoint
export const API_HOST = process.env.NODE_ENV === 'production' ? 'https://api.deepso.io/api' : '/api'

// mixpanel
export const MIXPANEL_KEY = process.env.NODE_ENV === 'production' ? '5dc73279b912dfb47e8b606d61ac2d53' : '9669b60b3252881b5c5673873bf0ca6c'

// google analytics
export const GA_KEY = process.env.NODE_ENV === 'production' ? 'UA-90985730-1' : false

// onesignal
export const ONESIGNAL_APPID = process.env.NODE_ENV === 'production' ? '66992542-87e8-4248-b152-3b33c5eb4c70' : '6cf6eef1-6775-4333-bfcd-0de79e5c9966'
export const ONESIGNAL_SAFARI_ID = process.env.NODE_ENV === 'production' ? 'web.onesignal.auto.0e731bf1-0f8d-4c8c-8593-03e4c907000a' : 'web.onesignal.auto.02091d44-51df-4a38-967b-45e7d1e7feaf'
